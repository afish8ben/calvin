define(['./app'], function(app){
   'use strict';
    return app.config(['$routeProvider', function($routeProvider){
        $routeProvider.when('/devices',{
            templateUrl: 'templates/device-temp.html',
            controller:'deviceCtrl'
        });

        $routeProvider.when('/health',{
            templateUrl: 'templates/health-temp.html',
            controller:'healthCtrl'
        });

        $routeProvider.otherwise({
            redirectTo:'/devices'
        });
    }]);
});