/**
 * Created by benjamin on 6/10/14.
 */
define(['./module'], function(controllers){
    'use strict';
    controllers.controller('deviceCtrl', ['$scope', '$http', function($scope, $http){
        $scope.message = 'no data yet';
        $scope.loaded_method = '';
        $scope.deviceIp = "";
        $scope.method = "";
        $scope.loaded = false;

        $scope.$watch('deviceIp', function(newValue){
            $scope.deviceIp = newValue;
        });

        $scope.$watch('method', function(nv){
            $scope.method = nv;
        });

        $scope.goButton = function(){
            console.log($scope.message);
            $scope.loaded_method = $scope.method;
            $scope.loaded = false;
            if($scope.deviceIp !== "") {
                try {
                    $http.get('http://' + $scope.deviceIp + ':8080/' + $scope.method).
                        success(function (data) {
                            $scope.message = data;
                            $scope.loaded = true;
                        });
                } catch (error) {
                    console.error(error);
                }
            }
        };
    }]);
});
