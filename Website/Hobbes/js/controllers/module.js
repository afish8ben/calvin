/**
 * Created by benjamin on 6/10/14.
 */
define(['angular'], function(ng){
    'use strict';
    return ng.module('app.controllers', []);
});
