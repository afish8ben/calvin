/**
 * Created by benjamin on 6/10/14.
 */
define(['./module'], function(filters){
    'use strict';
    return filters.filter('interpolate', ['version', function(version){
        return function(text){
            return String(text).replace(/\%VERSION\%/mg, version);
        };
    }]);
});
