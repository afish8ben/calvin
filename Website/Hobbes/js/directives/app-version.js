/**
 * Created by benjamin on 6/10/14.
 */
define(['./module'], function(directives){
    'use strict';
    directives.directive('appVersion', ['version', function(version){
        return function(scope, elm){
            elm.text(version);
        };
    }]);
});
