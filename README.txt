Top Hierarchy of the Calvin Webserver application
Contents:
___________________________
 - AndroidApplication: Contains Android Studio Application of the Calvin Webserver.
 - Website: Contains WebServer which will be used to connect to any Calvin server.

#### Android Client ####

Really the only thing working in this project as it was shut down for security reasons so there was no real hope to get it deployed.  The client though runs on the Android phone and serves up device information over an HTTP connection.

Current methods:
 - /location : Returns GPS coordinates of device.
 - /dirInfo : Opens directory and display list of files inside.
 - /systemInfo : Returns system information. 
 - /fileInfo : Returns information about a particular file.
 - /processInfo : Returns running process information.