package com.barrett.calvin.app;

import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import com.barrett.calvin.utils.FileObject;
import com.barrett.calvin.utils.GpsObject;
import com.barrett.calvin.utils.ProcessUtility;
import com.barrett.calvin.utils.SystemObject;
import com.google.gson.Gson;
import fi.iki.elonen.NanoHTTPD;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by benjamin on 5/29/14.
 */
public class CalvinServer extends NanoHTTPD {

    //METHOD NAMES
    final String FILE_INFO = "/fileInfo";
    final String SESSION_INFO = "/sessionInfo";
    final String SYSTEM_INFO = "/systemInfo";
    final String OPEN_DIRECTORY = "/dirInfo";
    final String GET_LOCATION = "/location";
    final String GET_PROCESSES = "/processInfo";
    final String SNAP_VIDEO = "/snap";

    //Server Name
    final String SERVER_NAME = "Calvin";

    //Context
    private Context mContext;

    //Gson
    private Gson gson = new Gson();

    //Constructor
    public CalvinServer(Context ctx){
        super(8080);
        mContext = ctx;
    }

    @Override
    public Response serve(IHTTPSession session){
        Method method = session.getMethod();
        String uri = session.getUri();
        Map<String, String> parameters = session.getParms();
        String msg = "";

        Log.d(SERVER_NAME, "method: " + method + " Uri: " + uri);
        //GET Methods
        if(uri.equals("/")){
            msg = loadIndex();
        }else if(uri.equals(FILE_INFO)){ //File Info
            msg = getFileInfo(session.getParms());
        }else if(uri.equals(SESSION_INFO)){ //Session Info
            msg = getSessionInfo();
        }else if(uri.equals(SYSTEM_INFO)){ //System Info
            msg = getSystemInfo();
        }else if(uri.equals(OPEN_DIRECTORY)){ //Open directory
            msg = openDirectory(session.getParms());
        }else if(uri.equals(GET_LOCATION)){ //Get Fine Location
            msg = getLocation();
        }else if(uri.equals(GET_PROCESSES)){
            msg = ProcessUtility.getRunningProcesses(mContext);
        } else if(uri.equals(SNAP_VIDEO)) {
            String videoUrl = parameters.get("video");
            Log.d("CalvinServer", "This is silly but here is the video id: " + videoUrl);
            loadVideo(videoUrl);
        }
        else{
            msg = loadIndex();
        }

        Log.d(SERVER_NAME, "MESSAGE: " + msg);
        NanoHTTPD.Response response = new Response(msg);
        //Need CORS here.
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "POST, GET, PUT");
        response.addHeader("Access-Control-Max-Age", "3600");
        response.addHeader("Access-Control-Allow-Headers", "x-requested-with");
        return response;
    }

    private void loadVideo(String videoId) {
        Intent mIntent = new Intent(mContext, VideoActivity.class);
        mIntent.putExtra("videoId", videoId);
        mContext.startActivity(mIntent);
    }

    private String loadIndex(){
        InputStream iStream = mContext.getResources().openRawResource(R.raw.index);

        InputStreamReader iReader = new InputStreamReader(iStream);
        BufferedReader bReader = new BufferedReader(iReader);
        String line;
        StringBuilder text = new StringBuilder();
        try{
            while((line = bReader.readLine()) != null){
                text.append(line);
                text.append("\n");
            }
        }catch(IOException ex){
            Log.e(SERVER_NAME, ex.getMessage());
        }

        return text.toString();
    }

    //TODO serve file information on path
    //@path /fileInfo
    //@method GET
    //@Parms path= Full path to the specified file.
    private String getFileInfo(Map<String, String> parms){
        String result;
        if(parms.containsKey("path")){
            File t = new File(parms.get("path"));
            FileObject fObject = new FileObject(t);
            result = gson.toJson(fObject);
        }else{
            result = "{error:\"File not found\"}";
        }
        return result;
    }

    //TODO serve session information of device
    //@path /sessionInfo
    //@method GET
    private String getSessionInfo(){
        return "";
    }

    //TODO serve system information on device
    //@path /systemInfo
    //@method GET
    private String getSystemInfo(){
        SystemObject systemInfo = new SystemObject();
        return gson.toJson(systemInfo);
    }

    //TODO Open Directory
    //@path /dirInfo
    //@method GET
    private String openDirectory(Map<String, String> parms){
        File t = new File(parms.get("directory"));
        List<FileObject> results = new ArrayList<FileObject>();
        if(t.isDirectory()){
            File[] temp = t.listFiles();
            for(File f : temp){
                results.add(new FileObject(f));
            }
        }

        return gson.toJson(results);
    }

    //@path /location
    //@method GET
    private String getLocation(){
        LocationManager locationManager = (LocationManager)
                mContext.getSystemService(Context.LOCATION_SERVICE);
        String mLocProvider;
        Criteria hdCrit = new Criteria();
        hdCrit.setAccuracy(Criteria.ACCURACY_COARSE);
        mLocProvider = locationManager.getBestProvider(hdCrit, true);

        Location current = locationManager.getLastKnownLocation(mLocProvider);
        GpsObject loc = new GpsObject(current);

        return gson.toJson(loc);
    }

    //path /upload
    //@method POST
    private void postVideoAndRun() {
        //TODO
    }
}
