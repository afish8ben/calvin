package com.barrett.calvin.app;

import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD;

/**
 * Created by barrett on 10/29/15.
 */
public class CalvinVideoServer extends NanoHTTPD {
    long fileLength;

    public CalvinVideoServer(int port, long fileLength) {
        super(port);
        this.fileLength = fileLength;
    }

    private Response getPartialResponse(String mimeType, String rangeHeader) throws IOException {
        File file = new File("/mnt/sdcard", "1-pumpsnap.mp4");
        String rangeValue = rangeHeader.trim().substring("bytes=".length());
        long fileLength = file.length();
        long start, end;
        if(rangeValue.startsWith("-")) {
            end = fileLength - 1;
            start = fileLength - 1
                    - Long.parseLong(rangeValue.substring("-".length()));
        } else {
            String[] range = rangeValue.split("-");
            start = Long.parseLong(range[0]);
            end = range.length > 1 ? Long.parseLong(range[1]) : fileLength - 1;
        }
        if (end >fileLength - 1) {
            end = fileLength - 1;
        }
        if(start <= end) {
            long contentLength = end - start + 1;
            FileInputStream fis = new FileInputStream(file);
            fis.skip(start);
            Response response = new Response(Response.Status.PARTIAL_CONTENT, mimeType, fis);
            response.addHeader("Content-Length", contentLength + "");
            response.addHeader("Content-Range", "bytes " + start + "-" + end + "/" + fileLength);
            Log.e("SERVER_PARTIAL", "bytes " + start + "-" + end + "/" + fileLength);
            response.addHeader("Content-Type", mimeType);
            return response;
        } else {
            return new Response(Response.Status.RANGE_NOT_SATISFIABLE, "video/mp4", rangeHeader);
        }
    }

    @Override
    public Response serve(String uri, Method method, Map<String, String> headers, Map<String, String> parms, Map<String, String> files ) {
        Log.d("CalvinVideoServer", "Hello. method" + method.name());
        long range;
        int constantLength = 307200;
        long fileLength = 0;
        boolean isLastPart = false;
        String rangeHeaderString = "";

        if(headers.containsKey("range")) {
            String contentRange = headers.get("range");
            range = Integer.parseInt(contentRange.substring(contentRange.indexOf("=") + 1, contentRange.indexOf("-")));
        } else {
            range = 0;
        }

        byte[] buffer;

        long buffLength = 0;

        try {
            RandomAccessFile ff = new RandomAccessFile(new File("/mnt/sdcard", "1-pumpsnap.mp4"), "rw");
            long remainingChunk = ff.length() - range;
            fileLength = ff.length();
            if(remainingChunk < constantLength) {
                buffLength = remainingChunk;
                isLastPart = true;
            } else {
                buffLength = constantLength;
            } if(range != 0) {
                ff.seek(range);
            }

            buffer = new byte[(int) buffLength];

            ff.read(buffer);
            rangeHeaderString = String.format("bytes=%s-%s", range, range+buffLength);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            buffer = new byte[0];
        } catch (IOException e) {
            e.printStackTrace();
            buffer = new byte[0];
        }

        Response response;

        response = new Response(Response.Status.PARTIAL_CONTENT, "video/mp4", new ByteArrayInputStream(buffer));
        response.addHeader("Content-Length", (fileLength) + "");
        response.addHeader("Content-Range", String.format("bytes %s-%s-%s", range, (range + buffLength), fileLength));
        Log.e("CalvinServer", "Inside Server sent " + String.format("bytes %s-%s-%s", range, (range + buffLength), fileLength));
        return response;

    }
}
