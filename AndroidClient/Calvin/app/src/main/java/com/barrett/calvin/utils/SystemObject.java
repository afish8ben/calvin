package com.barrett.calvin.utils;

import android.os.Build;

/**
 * Created by benjamin on 6/1/14.
 */
public class SystemObject {
    private int API_Level;
    private String Device;
    private String Model;
    private String Product;
    private String OS_Level;
    private String User;

    public SystemObject(){
        API_Level = Build.VERSION.SDK_INT;
        Device = Build.DEVICE;
        Model = Build.MODEL;
        Product = Build.PRODUCT;
        User = Build.USER;
        OS_Level = System.getProperty("os.version");
    }

    @Override
    public String toString(){
        return "SystemObject [API_Level="+API_Level+", Device="+Device+", Model="+Model+
                ", Product="+Product+", OS_Level="+OS_Level+"]";
    }
}
