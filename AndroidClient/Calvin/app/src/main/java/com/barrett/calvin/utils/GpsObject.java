package com.barrett.calvin.utils;

import android.location.Location;

/**
 * Created by benjamin on 6/1/14.
 */
public class GpsObject {

    private double longitude;
    private double latitude;

    public GpsObject(Location location){
        longitude = location.getLongitude();
        latitude = location.getLatitude();
    }

    @Override
    public String toString(){
        return "GpsObject [longitude="+longitude+", latitude="+latitude+"]";
    }
}
