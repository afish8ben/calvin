package com.barrett.calvin.utils;

import java.io.File;
import java.util.Date;

/**
 * Created by benjamin on 6/1/14.
 */
public class FileObject {
    private String fileName;
    private String path;
    private String fileSize;
    private Date date;
    private boolean directory;

    /*
    *Constructor
     */
    public FileObject(File file){
        fileName = file.getName();
        path = file.getAbsolutePath();
        long bytes = file.length();
        double fileKb = bytes / (1024);
        fileSize = fileKb + "KB";
        date = new Date(file.lastModified());
        directory = file.isDirectory();
    }

    /*
    *@method: getName
    *@return: String
    *@description: Returns the file name of the file.
     */
    public String getName(){
        return fileName;
    }

    /*
    *@method: getPath
    *@return: String
    *@description: Returns the absolute path of the file.
     */
    public String getPath(){
        return path;
    }

    /*
    *@method: getFileSize
    *@return: long
    *@description: Returns the file size of the specified file.
     */
    public String getFileSize(){
        return fileSize;
    }

    /*
    *@method: getDate
    *@return: long
    *@description: Returns the last modified of file.
     */
    public Date getDate(){
        return date;
    }

    @Override
    public String toString(){
        return "FileObject [fileName="+fileName+", path="+path+", fileSize="+fileSize+
                "]";
    }

}
