package com.barrett.calvin.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by benjamin on 6/9/14.
 */
public class ProcessUtility {

    private static Gson gson = new Gson();

    public static String getRunningProcesses(Context ctx){
        ActivityManager localActivityManager = (ActivityManager)
                ctx.getSystemService(Activity.ACTIVITY_SERVICE);

        List<ActivityManager.RunningServiceInfo> services = localActivityManager.getRunningServices(100);
        List<ProcessObject> processes = new ArrayList<ProcessObject>();

        for(ActivityManager.RunningServiceInfo rsi : services){
            ProcessObject t = new ProcessObject(
                    rsi.clientPackage,
                    rsi.process,
                    rsi.pid
            );
            processes.add(t);
        }

        return gson.toJson(processes);
    }

    private static class ProcessObject{
        String pkg, name;
        int pid;

        private ProcessObject(String pkg, String name, int pid){
            this.pkg = pkg;
            this.name = name;
            this.pid = pid;
        }

        public String toString(){
            return "PACKAGE: " + pkg + " NAME: " + name + " PID: " + pid;
        }
    }
}
