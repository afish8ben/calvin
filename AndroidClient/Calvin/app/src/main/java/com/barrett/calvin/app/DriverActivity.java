package com.barrett.calvin.app;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.widget.MediaController;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.VideoView;

import java.io.IOException;


public class DriverActivity extends ActionBarActivity {

    private CalvinServer server;
    private CalvinVideoServer videoServer;
    private TextView mView;
    private VideoView mVideo;
    private Button mBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver);

        server = new CalvinServer(this);

        try{
            server.start();
        }catch(Exception ex){
            Log.e("CALVIN", "Could not start server");
        }


        mView = (TextView)findViewById(R.id.text);
        //mVideo = (VideoView) findViewById(R.id.videoView);
        //mBtn = (Button) findViewById(R.id.snap_btn);

        //MediaController mediaController = new MediaController(this);

        displayIpAddress();
        //setUpVideoView(mediaController);

    }

    private void displayIpAddress(){
        WifiManager wifi = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        WifiInfo wInfo = wifi.getConnectionInfo();
        int ipAddress = wInfo.getIpAddress();
        String ip = String.format("%d.%d.%d.%d",
                (ipAddress & 0xff),(ipAddress >> 8 & 0xff),
                (ipAddress >> 16 & 0xff),
                (ipAddress >> 24 & 0xff));
        mView.setText("Server IP: " + ip + "\n" +
                "PORT: 8080");
    }

    private void setUpVideoView(MediaController mediaController){
        mVideo.setMediaController(mediaController);
        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startVideoServer(1);
            }
        });
    }

    public void startVideoServer(long fileLength) {
        WifiManager wifi = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        WifiInfo wInfo = wifi.getConnectionInfo();
        int ipAddress = wInfo.getIpAddress();
        String ip = String.format("%d.%d.%d.%d",
                (ipAddress & 0xff),(ipAddress >> 8 & 0xff),
                (ipAddress >> 16 & 0xff),
                (ipAddress >> 24 & 0xff));


        videoServer = new CalvinVideoServer(9090, fileLength);
        try {
            videoServer.start();
        } catch(IOException e) {
            e.printStackTrace();
        }

        Log.d("DriverActivity",  "Ip Address: " + ip);
        mVideo.setVideoURI(Uri.parse("http://" + ip + ":9090/1.mp4"));
        mVideo.requestFocus();
        mVideo.start();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if(server != null){
            server.stop();
        }

        if(videoServer != null) {
            videoServer.stop();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.driver, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
