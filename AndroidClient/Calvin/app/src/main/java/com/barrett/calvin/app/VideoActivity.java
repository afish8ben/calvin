package com.barrett.calvin.app;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

/**
 * Created by barrett on 10/30/15.
 */
public class VideoActivity extends Activity {

    WebView mWeb;

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_video);

        mWeb = (WebView) findViewById(R.id.videoView);

        String videoId = getIntent().getExtras().getString("videoId");
        String frameVideo = "<html><body>Snapumkin<br> <iframe width=\"320\" height=\"315\" src=\"https://www.youtube.com/embed/" + videoId + "?autoplay=1\" frameborder=\"0\" allowfullscreen></iframe></body></html>";


        mWeb.getSettings().setJavaScriptEnabled(true);
        mWeb.getSettings().setPluginState(WebSettings.PluginState.ON);
        mWeb.loadData(frameVideo, "text/html", "utf-8");
    }
}
